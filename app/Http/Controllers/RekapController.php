<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Excel;
use Illuminate\Support\Facades\Session;
class RekapController extends Controller
{

    public function rekap_pengeluaran($tgl,$mitra,$pid){
        $start = explode(':', $tgl)[0];
        $end = explode(':', $tgl)[1];
        $sql = $sqlm = $sqlpid  =  '';
        if($mitra != 'all'){
            $sqlm = " and MITRA = '".$mitra."'";
            $sql .= $sqlm;
        }
        if($pid != 'all'){
            $sqlpid .= " and PID = '".$pid."'";
            $sql .= $sqlpid;
        }
        $rfc = DB::select("SELECT COUNT(*) AS `Baris`, `RFC` FROM `logistik_rfc_item` where (TGL BETWEEN '".$start."' AND '".$end."') ".$sql." GROUP BY `RFC` ORDER BY `RFC`");

        $mitra = DB::select("SELECT `MITRA` as id, `MITRA` as text FROM `logistik_rfc_item` where (TGL BETWEEN '".$start."' AND '".$end."') GROUP BY `MITRA` ORDER BY `MITRA`");
        $pid = DB::select("SELECT `PID` as id, `PID` as text FROM `logistik_rfc_item` where (TGL BETWEEN '".$start."' AND '".$end."') ".$sqlm." GROUP BY `PID` ORDER BY `PID`");
        $data = DB::select("SELECT ID_BARANG,
        sum(case when NAMA_GUDANG = 'WH Banjarmasin' then BERI else 0 end) as WH_BJM,
        sum(case when NAMA_GUDANG = 'WH SO Banjarbaru' then BERI else 0 end) as WHSO_BJB,
        sum(case when NAMA_GUDANG = 'WH SO Banjarmasin A.Yani' then BERI else 0 end) as WHSO_BJM2,
        sum(case when NAMA_GUDANG = 'WH SO Banjarmasin Centrum' then BERI else 0 end) as WHSO_BJM1,
        sum(case when NAMA_GUDANG = 'WH SO Batulicin' then BERI else 0 end) as WHSO_BLC,
        sum(case when NAMA_GUDANG = 'WH SO Tabalong' then BERI else 0 end) as WHSO_TJL,
        sum(case when NAMA_GUDANG = 'WH SO Barabai' then BERI else 0 end) as WHSO_BRI,
        sum(case when NAMA_GUDANG = 'WH SO Kotabaru2' then BERI else 0 end) as WHSO_KPL,
        sum(case when NAMA_GUDANG = 'WH SO Pelaihari' then BERI else 0 end) as WHSO_PLE,
        sum(case when NAMA_GUDANG = 'WH SO Satui' then BERI else 0 end) as WHSO_STI,
        sum(case when NAMA_GUDANG = 'WH SO Rantau' then BERI else 0 end) as WHSO_RTA,
        sum(case when NAMA_GUDANG = 'WH SO Amuntai' then BERI else 0 end) as WHSO_AMT,
        sum(case when NAMA_GUDANG = 'WH SO Kandangan' then BERI else 0 end) as WHSO_KDG

        FROM logistik_rfc_item where (TGL BETWEEN '".$start."' AND '".$end."') ".$sql." GROUP BY ID_BARANG ORDER BY ID_BARANG");

        $mitra = DB::select("SELECT `MITRA` as id, `MITRA` as text FROM `logistik_rfc_item` where (TGL BETWEEN '".$start."' AND '".$end."') GROUP BY `MITRA` ORDER BY `MITRA`");
        $pid = DB::select("SELECT `PID` as id, `PID` as text FROM `logistik_rfc_item` where (TGL BETWEEN '".$start."' AND '".$end."') ".$sqlm." GROUP BY `PID` ORDER BY `PID`");
        return view('rekappengeluaran', compact('data', 'mitra', 'pid', 'rfc'));
    }
    public function download($tgl,$mitra,$pid){
        return Excel::download(new RFCExport($tgl,$mitra,$pid), 'MATERIAL_'.str_replace(":","-",$tgl).'_'.$mitra.'_'.$pid.'.xlsx');
    }
    public function downloadpdfrar($tgl,$mitra,$pid){
        //4902587382
        //4902587390
        $start=explode(':', $tgl)[0];
        $end=explode(':', $tgl)[1];

        $sql="(TGL BETWEEN '".$start."' AND '".$end."')";
        if($mitra != 'all'){
            $sql .= " and MITRA = '".$mitra."'";
        }
        if($pid != 'all'){
            $sql .= " and PID = '".$pid."'";
        }
        $data = DB::table('logistik_rfc_item')->whereRaw($sql)->orderBy('RFC','DESC')
          ->get();
        if(count($data)){
            $zip_file = 'MATERIAL_'.str_replace(":","-",$tgl).'_'.$mitra.'_'.$pid.'.zip'; // Name of our archive to download
            // $f=[];
            // Initializing PHP class
            $zip = new \ZipArchive();
            $zip->open($zip_file, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);
            foreach($data as $d){
                $folder = $d->MITRA?:'NoMitra';
                $folder .= '/'.$d->PID.'/'.$d->RFC.'.pdf';
                // $f[]=$folder;
                if (file_exists(public_path().'/storage/rfc_ttd/'.$d->RFC.'.pdf')) {
                    $zip->addFile(public_path().'/storage/rfc_ttd/'.$d->RFC.'.pdf', $folder);
                }
                else
                {
                    $zip->addFile(public_path().'/storage2/rfc_ttd/'.$d->RFC.'.pdf', $folder);
                }
            }
            // dd($f);
            $zip->close();

            // We return the file immediately after download
            return response()->download($zip_file);
        }
    }
    public function rekap_pengeluaran_by_rfc($bln){
        $sql = '1';
        $sqlinv = "type='Out'";
        if($bln){
            $sql = "TGL LIKE '".$bln."%'";
            $sqlinv .= " and posting_datex LIKE '".$bln."%'";
        }
        $data = DB::select("SELECT posting_datex,
          COUNT(DISTINCT case when plant = 'Banjarmasin' then no_rfc end) as WH_BJM_RFC,
          COUNT(DISTINCT case when plant = 'SO Banjarbaru' then no_rfc end) as WHSO_BJB_RFC,
          COUNT(DISTINCT case when plant = 'SO Banjarmasin A.Yani' then no_rfc end) as WHSO_BJM2_RFC,
          COUNT(DISTINCT case when plant = 'SO Banjarmasin Centrum' then no_rfc end) as WHSO_BJM1_RFC,
          COUNT(DISTINCT case when plant = 'SO Batulicin' then no_rfc end) as WHSO_BLC_RFC,
          COUNT(DISTINCT case when plant = 'SO Tabalong' then no_rfc end) as WHSO_TJL_RFC,
          COUNT(DISTINCT case when plant = 'SO Kotabaru2' then no_rfc end) as WHSO_KPL_RFC,
          COUNT(DISTINCT case when plant = 'SO Barabai' then no_rfc end) as WHSO_BRI_RFC,
          COUNT(DISTINCT case when plant = 'SO Pelaihari' then no_rfc end) as WHSO_PLE_RFC,
          COUNT(DISTINCT case when plant = 'SO Rantau' then no_rfc end) as WHSO_RTA_RFC,
          COUNT(DISTINCT case when plant = 'SO Satui' then no_rfc end) as WHSO_STI_RFC,
          COUNT(DISTINCT case when plant = 'SO Amuntai' then no_rfc end) as WHSO_AMT_RFC,
          COUNT(DISTINCT case when plant = 'SO Kandangan' then no_rfc end) as WHSO_KDG_RFC,
          COUNT(DISTINCT case when plant in ('SO Barabai', 'SO Tabalong', 'SO Batulicin', 'SO Pelaihari', 'SO Rantau', 'SO Satui', 'SO Amuntai', 'SO Kandangan', 'SO Banjarmasin Centrum', 'SO Banjarmasin A.Yani', 'SO Banjarbaru', 'Banjarmasin', 'SO Kotabaru2') then no_rfc end) as TOTAL_RFC
        FROM inventory_material WHERE ".$sqlinv." GROUP BY posting_datex ORDER BY posting_datex desc");
        $invarray = [];
        foreach($data as $d){
            $invarray[$d->posting_datex] = $d;
        }

        $data = DB::select("SELECT TGL,
          COUNT(DISTINCT case when NAMA_GUDANG = 'WH Banjarmasin' then RFC end) as WH_BJM_RFC,
          COUNT(DISTINCT case when NAMA_GUDANG = 'WH Banjarmasin' and FILE_RFC_TTD != '' then RFC end) as WH_BJM_TTD,

          COUNT(DISTINCT case when NAMA_GUDANG = 'WH SO Banjarbaru' then RFC end) as WHSO_BJB_RFC,
          COUNT(DISTINCT case when NAMA_GUDANG = 'WH SO Banjarbaru' and FILE_RFC_TTD != '' then RFC end) as WHSO_BJB_TTD,

          COUNT(DISTINCT case when NAMA_GUDANG = 'WH SO Banjarmasin A.Yani' then RFC end) as WHSO_BJM2_RFC,
          COUNT(DISTINCT case when NAMA_GUDANG = 'WH SO Banjarmasin A.Yani' and FILE_RFC_TTD != '' then RFC end) as WHSO_BJM2_TTD,

          COUNT(DISTINCT case when NAMA_GUDANG = 'WH SO Banjarmasin Centrum' then RFC end) as WHSO_BJM1_RFC,
          COUNT(DISTINCT case when NAMA_GUDANG = 'WH SO Banjarmasin Centrum' and FILE_RFC_TTD != '' then RFC end) as WHSO_BJM1_TTD,

          COUNT(DISTINCT case when NAMA_GUDANG = 'WH SO Batulicin' then RFC end) as WHSO_BLC_RFC,
          COUNT(DISTINCT case when NAMA_GUDANG = 'WH SO Batulicin' and FILE_RFC_TTD != '' then RFC end) as WHSO_BLC_TTD,

          COUNT(DISTINCT case when NAMA_GUDANG = 'WH SO Tabalong' then RFC end) as WHSO_TJL_RFC,
          COUNT(DISTINCT case when NAMA_GUDANG = 'WH SO Tabalong' and FILE_RFC_TTD != '' then RFC end) as WHSO_TJL_TTD,

          COUNT(DISTINCT case when NAMA_GUDANG = 'WH SO Kotabaru2' then RFC end) as WHSO_KPL_RFC,
          COUNT(DISTINCT case when NAMA_GUDANG = 'WH SO Kotabaru2' and FILE_RFC_TTD != '' then RFC end) as WHSO_KPL_TTD,

          COUNT(DISTINCT case when NAMA_GUDANG = 'WH SO Pelaihari' then RFC end) as WHSO_PLE_RFC,
          COUNT(DISTINCT case when NAMA_GUDANG = 'WH SO Pelaihari' and FILE_RFC_TTD != '' then RFC end) as WHSO_PLE_TTD,

          COUNT(DISTINCT case when NAMA_GUDANG = 'WH SO Satui' then RFC end) as WHSO_STI_RFC,
          COUNT(DISTINCT case when NAMA_GUDANG = 'WH SO Satui' and FILE_RFC_TTD != '' then RFC end) as WHSO_STI_TTD,

          COUNT(DISTINCT case when NAMA_GUDANG = 'WH SO Barabai' then RFC end) as WHSO_BRI_RFC,
          COUNT(DISTINCT case when NAMA_GUDANG = 'WH SO Barabai' and FILE_RFC_TTD != '' then RFC end) as WHSO_BRI_TTD,

          COUNT(DISTINCT case when NAMA_GUDANG = 'WH SO Rantau' then RFC end) as WHSO_RTA_RFC,
          COUNT(DISTINCT case when NAMA_GUDANG = 'WH SO Rantau' and FILE_RFC_TTD != '' then RFC end) as WHSO_RTA_TTD,

          COUNT(DISTINCT case when NAMA_GUDANG = 'WH SO Amuntai' then RFC end) as WHSO_AMT_RFC,
          COUNT(DISTINCT case when NAMA_GUDANG = 'WH SO Amuntai' and FILE_RFC_TTD != '' then RFC end) as WHSO_AMT_TTD,

          COUNT(DISTINCT case when NAMA_GUDANG = 'WH SO Kandangan' then RFC end) as WHSO_KDG_RFC,
          COUNT(DISTINCT case when NAMA_GUDANG = 'WH SO Kandangan' and FILE_RFC_TTD != '' then RFC end) as WHSO_KDG_TTD
        FROM logistik_rfc_item WHERE ".$sql." GROUP BY TGL ORDER BY TGL desc");

        return view('rekaprfc', compact('data', 'invarray'));
    }

    public function ajaxrekaprfc($tgl,$so,$jenis){
        $sql = '';
        $sql2 = "type='Out'";
        if($tgl){
            $sql .= 'TGL="'.$tgl.'"';
            $sql2 .= ' and posting_datex ="'.$tgl.'"';
        }
        if($so){
            $sql .= ' and NAMA_GUDANG="'.$so.'"';
            $sql2 .= ' and plant="'.$so.'"';
        }
        if($jenis == 'TTD'){
            $sql .= ' and FILE_RFC_TTD!=""';
        }
        if($jenis == 'ALISTA'){
            $data = DB::select("SELECT `no_rfc`,(select count(RFC) from logistik_rfc_item where RFC=no_rfc) as existed FROM `inventory_material` where ".$sql2." GROUP BY `no_rfc` ORDER BY `no_rfc`");
            // dd($data);
            return view('ajaxlistonlyrfc', compact('data'));
        }else{
            $data = DB::select('SELECT * FROM logistik_rfc_item lri WHERE '.$sql.' order by FILE_RFC_TTD asc');

            $lastrfc = '';
            $dataarray =[];
            foreach ($data as $no => $m){
              if($lastrfc == $m->RFC){
                $dataarray[$m->RFC]->list[] = ['ID_BARANG'=>$m->ID_BARANG,'NAMA_BARANG'=>$m->NAMA_BARANG,'SATUAN'=>$m->SATUAN,'MINTA'=>$m->MINTA,'BERI'=>$m->BERI];

              }else{
                $m->list[] = ['ID_BARANG'=>$m->ID_BARANG,'NAMA_BARANG'=>$m->NAMA_BARANG,'SATUAN'=>$m->SATUAN,'MINTA'=>$m->MINTA,'BERI'=>$m->BERI];
                $dataarray[$m->RFC] = $m;
              }
              $lastrfc = $m->RFC;
            }
            // dd($dataarray);
            return view('ajaxlist', compact('dataarray'));
        }
    }
}
