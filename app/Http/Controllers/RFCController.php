<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Session;
class RFCController extends Controller
{
    public function home(){
        return view('home');
    }
    public function listrfc(Request $req, $tgl = null)
    {

        $sql = '1';
        if($req->q){
            $sql = 'RFC='.$req->q;
        }
        if($tgl){
            $start=explode(':', $tgl)[0];
            $end=explode(':', $tgl)[1];
            $sql = 'TGL BETWEEN "'.$start.'" AND "'.$end.'"';
        }
        $data = DB::select('SELECT * FROM logistik_rfc_item lri WHERE '.$sql.' order by RFC desc');
        $lastrfc = '';
        $dataarray =[];
        foreach ($data as $no => $m){
          if($lastrfc == $m->RFC){
            $dataarray[$m->RFC]->list[] = ['ID_BARANG'=>$m->ID_BARANG,'NAMA_BARANG'=>$m->NAMA_BARANG,'SATUAN'=>$m->SATUAN,'MINTA'=>$m->MINTA,'BERI'=>$m->BERI];

          }else{
            $m->list[] = ['ID_BARANG'=>$m->ID_BARANG,'NAMA_BARANG'=>$m->NAMA_BARANG,'SATUAN'=>$m->SATUAN,'MINTA'=>$m->MINTA,'BERI'=>$m->BERI];
            $dataarray[$m->RFC] = $m;
          }
          $lastrfc = $m->RFC;
        }
        // dd($dataarray);
        return view('list', compact('dataarray'));
    }
    public function uploadForm()
    {
        return view('upload');
    }
    public function uploadSave(Request $req)
    {
        // dd($req->debug);
        $msg = $this->handleFileUpload($req);

        return redirect('/rfc/'.date('Y-m-d').':'.date('Y-m-d'))->with('alertblock', [
            $msg,
        ]);
    }
    public function uploadFormTTD()
    {
        return view('uploadttd');
    }
    public function uploadSaveTTD(Request $req)
    {

        $path = public_path().'/storage2/rfc_ttd/';
        if (! file_exists($path)) {
            if (! mkdir($path, 0770, true)){
                return [
                    'type' => 'danger',
                    'text' => 'Gagal Menyiapkan Folder'
                ];
            }
        }
        foreach($req->file('rfc') as $file){
            $name = $file->getClientOriginalName();
            try {
                $moved = $file->move("$path", $name);
                $rfc = explode('.', $name)[0];
                // $rfc = str_replace('.pdf','', $name);
                DB::table('logistik_rfc_item')->where('RFC', $rfc)->update(['FILE_RFC_TTD'=>$name]);
            } catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
                return [
                    'type' => 'danger',
                    'text' => 'Gagal Upload'
                ];
            }

            // $sql .= "UPDATE `logistik_rfc_item` SET `FILE_RFC_TTD` = `".$name."` WHERE `RFC` = `".explode('.', $name)[0]."`;";
        }
        // DB::update(DB::raw($sql));
        // @DB::statement($sql);
        // dd($sql);
        return redirect('/rfc/'.date('Y-m-d').':'.date('Y-m-d'))->with('alertblock', [
            [
                'type' => 'success',
                'text' => 'Berhasil Upload scan RFC Bertandatangan'
            ]
        ]);
    }

    private function handleFileUpload($req)
    {
        $path = public_path().'/storage2/rfc/';
        if (! file_exists($path)) {
            if (! mkdir($path, 0770, true)) {
                return [
                    'type' => 'danger',
                    'text' => 'Gagal Menyiapkan Folder'
                ];
            }
        }
        foreach ($req->file('rfc') as $file) {
            $name = $file->getClientOriginalName();
            try {
                $moved = $file->move("$path", $name);
            } catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
                return [
                    'type' => 'danger',
                    'text' => 'Gagal Upload'
                ];
            }
        }

        $toinsert=$rfcs=[];
        foreach ($req->file('rfc') as $file) {
            $name = $file->getClientOriginalName();
            $isipdf = $this->bacapdf($req, $name, $req->debug);
            if($isipdf['type']=='gagal'){
                return [
                    'type' => 'danger',
                    'text' => $isipdf['text']
                ];
            }else if($isipdf['type']=='success'){
                $rfcs[] = $isipdf['text'][0]['RFC'];
                $toinsert = array_merge($toinsert,$isipdf['text']);
            }
        }
                // dd($toinsert);

        DB::table('logistik_rfc_item')->whereIn('RFC', $rfcs)->delete();
        DB::table('logistik_rfc_item')->insert($toinsert);
        // dd($toinsert);
        return [
                'type' => 'success',
                'text' => 'Sukses Menyimpan Data'
            ];

    }
    private function bacapdf($req,$nama_file,$debug){
        $pdf = new \Gufy\PdfToHtml\Pdf(public_path().'/storage2/rfc/'.$nama_file);
        $pages = $pdf->getPages();
        $htmlasli= '';
        $htmlp = '';
        $readablearray=[];
        $arrayhtml = [];

            $lasttop=$lastleft='';
        for($i=1;$i<=$pages;$i++){
            $htmlp = $pdf->html($i);
            $htmlasli.=$htmlp;
            $html = str_replace('white-space:nowrap" class="ft01">','', trim($htmlp));
            $html = str_replace('white-space:nowrap" class="ft02">','', $html);
            $html = str_replace('white-space:nowrap" class="ft03">','', $html);
            $html = str_replace('white-space:nowrap" class="ft00">','', $html);

            $html = str_replace(':','', $html);
            $html = str_replace('<br>',' ', $html);
            $html = str_replace(' ',' ', $html);
            $html = str_replace('<p style="positionabsolute;','', $html);

            $exp = explode('</p>', $html);
            foreach($exp as $c=> $h){
              // print_r($c);
              // print_r(count($exp));
              if($c>=1 && $c<count($exp)-2){
                $p = explode(';', trim($h));

                if($p[1] == $lastleft){
                  $arrayhtml[$lasttop][$p[1]] .= ' '.$p[2];
                  // echo$p[1].'llll'.$lastleft."<br\>edit".$p[2];
                }else{

                  // echo"\ninp".$p[2];
                  $arrayhtml[$p[0]][$p[1]] = $p[2];
                  $lasttop=$p[0];
                  $lastleft=$p[1];
                }
              }
            }
            // if($i==2)dd($arrayhtml);
            // dd($htmlp,$arrayhtml);

            $keytop=0;
            foreach($arrayhtml as $ah){
              $keyleft=0;
              foreach($ah as $h){
                $readablearray[$keytop][$keyleft++]=$h;
              }
              $keytop++;
            }
        }
        // dd($readablearray,$htmlasli);
        // dd($readablearray);
        if(!$readablearray){
            return [
                'type' => 'gagal',
                'text' => 'Tidak Boleh Scan / Harus PDF ASLI dari SAP'
            ];
        }
        $rfc = $pid = $nama_project = $nama_gudang = $alamat = $mitra = $lokasi ='';
        if(count($readablearray[0])==2){
            $rfc = substr($readablearray[0][1],1);
            $pid = substr($readablearray[1][1],1);
            $nama_project = substr(@$readablearray[2][1],1);
            $nama_gudang = substr($readablearray[3][1],1);
            $alamat = substr(@$readablearray[4][1],1);
            $mitra = substr(@$readablearray[5][1],1);
            $lokasi = substr(@$readablearray[6][1],1);
        }else if(count($readablearray[0])==3){
            $rfc = $readablearray[0][2];
            $pid = $readablearray[1][2];
            $nama_project = @$readablearray[2][2];
            $nama_gudang = $readablearray[3][2];
            $alamat = @$readablearray[4][2];
            $mitra = @$readablearray[5][2];
            $lokasi = @$readablearray[6][2];
        }
        $rfcttd = '';
        if (file_exists(public_path().'/storage2/rfc_ttd/'.$nama_file)) {
            $rfcttd = $nama_file;
        }
        $head=[
            'WITEL'             =>$req->witel,
            'FILE_RFC'          =>$nama_file,
            'FILE_RFC_TTD'      =>$rfcttd,
            'RFC'               =>$rfc,
            'PID'               =>$pid,
            'NAMA_PROJECT'      =>$nama_project,
            'NAMA_GUDANG'       =>$nama_gudang,
            'ALAMAT_GUDANG'     =>$alamat,
            'MITRA'             =>$mitra,
            'LOKASI'            =>$lokasi
        ];
        $isi = $toinsert=[];
        $no=0;
        $format='';
        foreach($readablearray as $key => $r){
            if(count($r)==6 && $key>=9){
                $isi[] = [
                    'ID_BARANG'     =>$r[1],
                    'NAMA_BARANG'   =>$r[2],
                    'SATUAN'        =>$r[3],
                    'MINTA'         =>str_replace('.','', $r[4]),
                    'BERI'          =>str_replace('.','', $r[5])
                ];
                $no=$key;
                $format='A';
            }
            if(count($r)==5 && $key>=9){
                $isi[] = [
                    'ID_BARANG'     =>$r[1],
                    'NAMA_BARANG'   =>'',
                    'SATUAN'        =>$r[2],
                    'MINTA'         =>str_replace('.','', $r[3]),
                    'BERI'          =>str_replace('.','', $r[4])
                ];
                $no=$key;$format='B';
            }
        }
        if($no){
            $no++;
            // dd((count($readablearray)-$no),$readablearray);
            $last = count($readablearray);
            try{
                if((count($readablearray)-$no)==8){
                    if($pages>1){
                        $tgl = substr($readablearray[$no+7][0],7);
                        $foot=[
                            'MENYERAHKAN'   =>$readablearray[$no+2][0],
                            'MENERIMA'      =>$readablearray[$no+2][1],
                            'MENGETAHUI'    =>$readablearray[$no+5][0],
                            'TGL'           =>explode('.', $tgl)[2].'-'.explode('.', $tgl)[1].'-'.explode('.', $tgl)[0],
                            'ISI'           =>json_encode(array_slice($readablearray,$no))
                        ];

                    }else{
                        $tgl = substr($readablearray[$no+6][0],7);
                        $foot=[
                            'MENYERAHKAN'   =>$readablearray[$no+1][0],
                            'MENERIMA'      =>$readablearray[$no+1][1],
                            'MENGETAHUI'    =>$readablearray[$no+4][0],
                            'TGL'           =>explode('.', $tgl)[2].'-'.explode('.', $tgl)[1].'-'.explode('.', $tgl)[0],
                            'ISI'           =>json_encode(array_slice($readablearray,$no))
                        ];
                    }
                }else if((count($readablearray)-$no)==9){
                    $tgl = substr($readablearray[$no+7][0],7);
                    // dd($no,$tgl,$isi,$readablearray,$htmlp);
                    $foot=[
                        'MENYERAHKAN'   =>$readablearray[$no+2][0],
                        'MENERIMA'      =>$readablearray[$no+2][1],
                        'MENGETAHUI'    =>$readablearray[$no+5][0],
                        'TGL'           =>explode('.', $tgl)[2].'-'.explode('.', $tgl)[1].'-'.explode('.', $tgl)[0],
                        'ISI'           =>json_encode(array_slice($readablearray,$no))
                    ];
                }else{
                    $tgl = substr($readablearray[$no+5][0],7);
                    $foot=[
                        'MENYERAHKAN'   =>$readablearray[$no+1][0],
                        'MENERIMA'      =>$readablearray[$no+1][1],
                        'MENGETAHUI'    =>$readablearray[$no+3][0],
                        'TGL'           =>explode('.', $tgl)[2].'-'.explode('.', $tgl)[1].'-'.explode('.', $tgl)[0],
                        'ISI'           =>json_encode(array_slice($readablearray,$no))
                    ];
                }
            }catch(\Exception $e){
                $foot=[
                    'MENYERAHKAN'   =>'',
                    'MENERIMA'      =>'',
                    'MENGETAHUI'    =>'',
                    'TGL'           =>'',
                    'ISI'           =>json_encode($readablearray),
                ];
            }

            foreach($isi as $i){
                $toinsert[] = array_merge($head,$i,$foot);
            }
            if($debug){
                dd($tgl,$toinsert,$readablearray,$htmlp);
            }
            return [
                'type' => 'success',
                'text' => $toinsert
            ];
        }else{
            return [
                'type' => 'gagal',
                'text' => 'format pdf tidak sesuai, hub developer!'
            ];
        }
    }
}
