<?php
namespace App\Http\Controllers;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use DB;
class RFCExport implements FromView
{
    protected $sql;

    function __construct($tgl,$mitra,$pid) {
        $start=explode(':', $tgl)[0];
        $end=explode(':', $tgl)[1];

        $this->sql="(TGL BETWEEN '".$start."' AND '".$end."')";
        if($mitra != 'all'){
            $this->sql .= " and MITRA = '".$mitra."'";
        }
        if($pid != 'all'){
            $this->sql .= " and PID = '".$pid."'";
        }
    }
    public function view(): View
    {
        return view('excelviewdownload', [
            'data' => DB::table('logistik_rfc_item')->whereRaw($this->sql)->orderBy('RFC','DESC')
          ->get()
        ]);
    }
}