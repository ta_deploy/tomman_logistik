<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Session;
use File;
class RFRController extends Controller
{
    public function listrfr(Request $req, $tgl = null)
    {

        $sql = '1';
        if($req->q){
            $sql = 'RFR='.$req->q;
        }
        if($tgl){
            $start=explode(':', $tgl)[0];
            $end=explode(':', $tgl)[1];
            $sql = 'TGL BETWEEN "'.$start.'" AND "'.$end.'"';
        }
        // dd('SELECT * FROM logistik_rfr_item lri WHERE '.$sql.' order by RFR desc');
        $data = DB::select('SELECT * FROM logistik_rfr_item lri WHERE '.$sql.' order by RFR desc');
        $lastrfr = '';
        $dataarray =[];
        foreach ($data as $no => $m){
          if($lastrfr == $m->RFR){
            $dataarray[$m->RFR]->list[] = ['ID_BARANG'=>$m->ID_BARANG,'NAMA_BARANG'=>$m->NAMA_BARANG,'SATUAN'=>$m->SATUAN,'VOLUME'=>$m->VOLUME];

          }else{
            $m->list[] = ['ID_BARANG'=>$m->ID_BARANG,'NAMA_BARANG'=>$m->NAMA_BARANG,'SATUAN'=>$m->SATUAN,'VOLUME'=>$m->VOLUME];
            $dataarray[$m->RFR] = $m;
          }
          $lastrfr = $m->RFR;
        }
        // dd($dataarray);
        return view('rfr.list', compact('dataarray'));
    }
    public function uploadForm()
    {
        return view('rfr.upload');
    }
    public function uploadSave(Request $req)
    {
        // dd($req->debug);
        $msg = $this->handleFileUpload($req);

        return redirect('/rfr/'.date('Y-m-d').':'.date('Y-m-d'))->with('alertblock', [
            $msg,
        ]);
    }


    private function handleFileUpload($req)
    {
        $path = public_path().'/storage2/rfr/';
        if (! file_exists($path)) {
            if (! mkdir($path, 0770, true)) {
                return [
                    'type' => 'danger',
                    'text' => 'Gagal Menyiapkan Folder'
                ];
            }
        }
        foreach ($req->file('rfr') as $file) {
            $name = $file->getClientOriginalName();
            try {
                $moved = $file->move("$path", $name);
            } catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
                return [
                    'type' => 'danger',
                    'text' => 'Gagal Upload'
                ];
            }
        }

        $toinsert=$rfrs=[];
        foreach ($req->file('rfr') as $file) {
            $name = $file->getClientOriginalName();
            $isipdf = $this->bacapdf($req, $name, $req->debug);
            if($isipdf['type']=='gagal'){
                return [
                    'type' => 'danger',
                    'text' => $isipdf['text']
                ];
            }else if($isipdf['type']=='success'){
                $rfr = $isipdf['text'][0]['RFR'];
                $rfrs[] = $rfr;
                $toinsert = array_merge($toinsert,$isipdf['text']);
                File::move("$path/$name", "$path/$rfr.pdf");
                // dd($toinsert,$isipdf);
            }
        }
                // dd($toinsert);
        DB::table('logistik_rfr_item')->whereIn('RFR', $rfrs)->delete();
        DB::table('logistik_rfr_item')->insert($toinsert);

        // dd($toinsert);
        return [
                'type' => 'success',
                'text' => 'Sukses Menyimpan Data'
            ];

    }
    private function bacapdf($req,$nama_file,$debug){
        $pdf = new \Gufy\PdfToHtml\Pdf(public_path().'/storage2/rfr/'.$nama_file);
        $pages = $pdf->getPages();
        $htmlasli= '';
        $htmlp = '';
        $readablearray=[];
        $arrayhtml = [];
        $head = '';
        $lasttop=$lastleft='';
        for($i=1;$i<=$pages;$i++){
            $htmlp = $pdf->html($i);
            $htmlasli.=$htmlp;
            $head = explode('</p>', $htmlp);
            // dd($head);
            $exp = explode('<br>', str_replace('<p style="position:absolute;top:177px;left:187px;white-space:nowrap" class="ft02">', '<br>',$head[0]));
            $timestamp = date("Y-m-d", strtotime(str_replace('.', '-', explode('/', $exp[2])[1])));
            $head = ["RFR"=>$exp[1],"TGL"=>$timestamp,"PID"=>$exp[3],"NAMA_GUDANG"=>$exp[4]];
            // dd($head);

            $html = str_replace('white-space:nowrap" class="ft01">','', trim($htmlp));
            $html = str_replace('white-space:nowrap" class="ft02">','', $html);
            $html = str_replace('white-space:nowrap" class="ft03">','', $html);
            $html = str_replace('white-space:nowrap" class="ft00">','', $html);

            $html = str_replace(':','', $html);
            $html = str_replace('<br>',' ', $html);
            $html = str_replace(' ',' ', $html);
            $html = str_replace('<p style="positionabsolute;','', $html);
            // dd($html);
            $exp = explode('</p>', $html);
            // dd($exp[0],$htmlp);
            foreach($exp as $c=> $h){
              // print_r($c);
              // print_r(count($exp));
              if($c>=1 && $c<count($exp)-2){
                $p = explode(';', trim($h));

                if($p[1] == $lastleft){
                  $arrayhtml[$lasttop][$p[1]] .= ' '.$p[2];
                  // echo$p[1].'llll'.$lastleft."<br\>edit".$p[2];
                }else{

                  // echo"\ninp".$p[2];
                  $arrayhtml[$p[0]][$p[1]] = $p[2];
                  $lasttop=$p[0];
                  $lastleft=$p[1];
                }
              }
            }
            // if($i==2)dd($arrayhtml);
            // dd($htmlp,$arrayhtml);

            $keytop=0;
            foreach($arrayhtml as $ah){
              $keyleft=0;
              foreach($ah as $h){
                $readablearray[$keytop][$keyleft++]=$h;
              }
              $keytop++;
            }
        }

        // dd($readablearray,$htmlasli);
        $totalline = count($readablearray);
        // $readablearray[$totalline-4];
        $foot['MENGEMBALIKAN'] = $readablearray[$totalline-4][0];
        $foot['MENERIMA'] = $readablearray[$totalline-4][1];
        $foot['CB'] = session('auth')->id_user;
        if(!$readablearray){
            return [
                'type' => 'gagal',
                'text' => 'Tidak Boleh Scan / Harus PDF ASLI dari SAP'
            ];
        }

        $rfcttd = '';
        if (file_exists(public_path().'/storage2/rfc_ttd/'.$nama_file)) {
            $rfcttd = $nama_file;
        }
        $isi = $toinsert=[];
        $no=0;
        $format='';
        foreach($readablearray as $key => $r){
            if(count($r)==5 && $key>=3){
                $toinsert[] = array_merge($head,[
                    'ID_BARANG'     =>$r[1],
                    'NAMA_BARANG'   =>$r[2],
                    'SATUAN'        =>$r[4],
                    'VOLUME'         =>$r[3]
                ],$foot);
                $no=$key;$format='A';
            }
        }
        if($no){
            if($debug){
                dd($toinsert,$readablearray,$htmlp);
            }
            return [
                'type' => 'success',
                'text' => $toinsert
            ];
        }else{
            return [
                'type' => 'gagal',
                'text' => 'format pdf tidak sesuai, hub developer!'
            ];
        }
    }
}
