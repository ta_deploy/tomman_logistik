@extends('layout')
@section('css')
<style type="text/css">
    .no-search .select2-search {
        display:none
    }
    body .modal-xl {
        width: 1250px;
    }
</style>
@endsection
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>Material / </span>List
</h1>

<button id="daterange-4" class="btn dropdown-toggle"></button>
<a href="/uploadrfr" class="pull-right"><span class="btn btn-success m-r-1"><i class="ion ion-plus"></i> Upload RFR</span></a>
@endsection
@section('title', 'List Material')
@section('content')

<input type="hidden" name="start" id="start" value="{{ @explode(':', Request::segment(2))[0] }}">
<input type="hidden" name="end" id="end" value="{{ @explode(':', Request::segment(2))[1] }}">

<div class="panel">
  <div class="panel-body">
    <div class="table-responsive table-primary">
        <table class="table">
            <thead>
                <tr>
                    <th>INFO</th>
                    <th>PENGESAHAN</th>
                    <th>ID BARANG</th>
                    <th>VOLUME</th>
                </tr>
            </thead>
            <tbody>
                    <?php
                    $no=0;
                    ?>
                @foreach($dataarray as $key => $d)

                    <?php
                        $span = count($d->list);
                        $pathori = "/storage/rfr/$d->RFR.pdf";
                        $pathori2 = '/storage2/rfc/'.$d->FILE_RFC;
                    ?>
                    <tr>
                        <td rowspan="{{ $span }}">
                            <span class="label label-info">{{ ++$no }} . </span>
                            <span class="label label-primary">RFR : {{ $d->RFR }}</span><br>
                            <span class="label label-primary">PID : {{ $d->PID }}</span><br>
                            <span class="label label-primary">GUDANG : {{ $d->NAMA_GUDANG }}</span><br>
                        </td>
                        <td rowspan="{{ $span }}">
                            @if (file_exists(public_path().$pathori))
                                <a href="{{ $pathori }}" target="_blank"><span class="btn btn-xs btn-success"><i class="ion ion-ios-cloud-download"></i> Download RFR</span></a>
                            @else
                                <a href="{{ $pathori2 }}" target="_blank"><span class="btn btn-xs btn-success"><i class="ion ion-ios-cloud-download"></i> Download RFR</span></a>
                            @endif
                            <span class="label label-primary">TGL : {{ $d->TGL }}</span><br>
                            <span class="label label-primary">MENGEMBALIKAN : {{ $d->MENGEMBALIKAN }}</span><br>
                            <span class="label label-primary">MENERIMA : {{ $d->MENERIMA }}</span>

                        </td>
                        @foreach($d->list as $nos => $l)
                            @if($nos)
                            <tr>
                            @endif
                                <td class="valign-middle">
                                    <span class="label label-success">{{ $l['ID_BARANG'] }}</span> <span class="label label-default">{{ $l['SATUAN'] }}</span><br>
                                    <span class="label label-primary">{{ $l['NAMA_BARANG'] }}</span>
                                </td>
                                <td class="valign-middle"><span class="pull-right">{{ number_format($l['VOLUME']) }}</span></td>
                            </tr>
                        @endforeach

                @endforeach
            </tbody>
        </table>
    </div>
  </div>
</div>

@endsection

@section('js')
<script type="text/javascript">
    $(function() {
        var start = moment($('#start').val());
        var end = moment($('#end').val());

        function cb(start, end) {
          $('#daterange-4').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }

        $('#daterange-4').daterangepicker({
          startDate: start,
          endDate: end,
          ranges: {
           'Hari Ini': [moment(), moment()],
           'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           '7 Hari Terakhir': [moment().subtract(6, 'days'), moment()],
           '30 Hari Terakhir': [moment().subtract(29, 'days'), moment()],
           'Bulan Ini': [moment().startOf('month'), moment().endOf('month')],
           'Bulan Sebelumnya': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          }
        }, cb);

        cb(start, end);
        $('#daterange-4').on('apply.daterangepicker', function(ev, picker) {
            var s = picker.startDate.format('YYYY-MM-DD');
            var e = picker.endDate.format('YYYY-MM-DD');
            window.location.href = document.location.origin+"/rfc/"+s+":"+e;
        });
    });
</script>
@endsection
