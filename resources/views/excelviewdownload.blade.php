<table class="table table-bordered">
    <thead>
        <tr>
            <th>RFC</th>
            <th>PID</th>
            <th>PROJECT</th>
            <th>GUDANG</th>
            <th>MITRA</th>
            <th>LOKASI</th>
            <th>TGL</th>
            <th>MENYERAHKAN</th>
            <th>MENERIMA</th>
            <th>MENGETAHUI</th>
            <th>ID_BARANG</th>
            <th>NAMA_BARANG</th>
            <th>SATUAN</th>
            <th>MINTA</th>
            <th>BERI</th>
        </tr>
    </thead>
    <tbody>
        @foreach($data as $key => $d)
            <tr>
                <td>{{ $d->RFC }}</td>
                <td>{{ $d->PID }}</td>
                <td>{{ $d->NAMA_PROJECT }}</td>
                <td>{{ $d->NAMA_GUDANG }}</td>
                <td>{{ $d->MITRA }}</td>
                <td>{{ $d->LOKASI }}</td>
                <td>{{ $d->TGL }}</td>
                <td>{{ $d->MENYERAHKAN }}</td>
                <td>{{ $d->MENERIMA }}</td>
                <td>{{ $d->MENGETAHUI }}</td>
                <td>{{ $d->ID_BARANG }}</td>
                <td>{{ $d->NAMA_BARANG }}</td>
                <td>{{ $d->SATUAN }}</td>
                <td>{{ $d->MINTA }}</td>
                <td>{{ $d->BERI }}</td>
            </tr>
        @endforeach
    </tbody>
</table>