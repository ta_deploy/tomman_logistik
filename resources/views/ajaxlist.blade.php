<div class="table-responsive table-primary">
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>#</th>
                <th>INFO</th>
                <th>PENGESAHAN</th>
                <th>ID_BARANG</th>
                <th>SATUAN</th>
                <th>MINTA</th>
                <th>BERI</th>
            </tr>
        </thead>
        <tbody>
                <?php
                $no=0;
                ?>
            @foreach($dataarray as $key => $d)

                <?php
                    $span = count($d->list);
                    $path = '/storage/rfc_ttd/'.$d->RFC.'.pdf';
                    $pathori = '/storage/rfc/'.$d->FILE_RFC;
                    $path2 = '/storage2/rfc_ttd/'.$d->RFC.'.pdf';
                    $pathori2 = '/storage2/rfc/'.$d->FILE_RFC;
                ?>
                <tr>
                    <td rowspan="{{ $span }}">
                        {{ ++$no }}
                    </td>
                    <td rowspan="{{ $span }}">
                        <span class="label label-primary">RFC : {{ $d->RFC }}</span><br>
                        <span class="label label-primary">PID : {{ $d->PID }}</span><br>
                        <span class="label label-primary">PROJECT : {{ $d->NAMA_PROJECT }}</span><br>
                        <span class="label label-primary">GUDANG : {{ $d->NAMA_GUDANG }}</span><br>
                        <span class="label label-primary">MITRA : {{ $d->MITRA }}</span><br>
                        <span class="label label-primary">LOKASI : {{ $d->LOKASI }}</span>
                    </td>
                    <td rowspan="{{ $span }}">
                        @if (file_exists(public_path().$pathori) )
                            <a href="{{ $pathori }}" target="_blank"><span class="btn btn-xs btn-success"><i class="ion ion-ios-cloud-download"></i> Download RFC ASLI</span></a>
                        @else
                            <a href="{{ $pathori2 }}" target="_blank"><span class="btn btn-xs btn-success"><i class="ion ion-ios-cloud-download"></i> Download RFC ASLI</span></a>
                        @endif
                        @if (file_exists(public_path().$path))
                            <a href="{{ $path }}" target="_blank"><span class="btn btn-xs btn-success"><i class="ion ion-ios-cloud-download"></i> Download RFC TTD</span></a><br>
                        @else
                            <a href="{{ $path2 }}" target="_blank"><span class="btn btn-xs btn-success"><i class="ion ion-ios-cloud-download"></i> Download RFC TTD</span></a><br>
                        @else
                            <span class="label label-danger">RFC Bertandatangan BLM ADA</span><br>
                        @endif
                        <span class="label label-primary">TGL : {{ $d->TGL }}</span><br>
                        <span class="label label-primary">MENYERAHKAN : {{ $d->MENYERAHKAN }}</span><br>
                        <span class="label label-primary">MENERIMA : {{ $d->MENERIMA }}</span><br>
                        <span class="label label-primary">MENGETAHUI : {{ $d->MENGETAHUI }}</span>

                    </td>
                    @foreach($d->list as $nos => $l)
                        @if($nos)
                        <tr>
                        @endif
                            <td class="valign-middle">
                                <span class="label label-success">{{ $l['ID_BARANG'] }}</span><br>
                                <span class="label label-primary">{{ $l['NAMA_BARANG'] }}</span>
                            </td>
                            <td class="valign-middle">{{ $l['SATUAN'] }}</td>
                            <td class="valign-middle"><span class="pull-right">{{ number_format($l['MINTA']) }}</span></td>
                            <td class="valign-middle"><span class="pull-right">{{ number_format($l['BERI']) }}</span></td>
                        </tr>
                    @endforeach

            @endforeach
        </tbody>
    </table>
</div>
