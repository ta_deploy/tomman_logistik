@extends('layout')
@section('heading')
<h1>
  <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>RFC SAP/ </span>upload
</h1>
@endsection
@section('title', 'Upload')
@section('content')
<div class="panel">
  <div class="panel-body">
<form class="form-horizontal" method="post" id="form-upload" enctype="multipart/form-data">
    <input type="hidden" name="witel" value="{{ session('auth')->Witel_New }}">
    <div class="form-group form-message-dark">
        <label for="RFC" class="col-md-2 control-label">RFC</label>
        <div class="col-md-10">
            <input type="file" id="rfc" class="form-control-file" name="rfc[]" required multiple>
            <small class="text-muted">RFC Asli dari SAP, yg bisa di block text nya.Tidak boleh hasil scan.</small>
        </div>
    </div>
    <div class="form-group form-message-dark form-group-sm {{ session('auth')->id_user=='wandiy99'?'':'hidden' }}">
        <label for="switcher-sm" class="col-md-2 control-label">Debug</label>
        <div class="col-md-9">
          <label for="switcher-sm" class="switcher switcher-blank switcher-success">
            <input type="checkbox" id="switcher-sm" name="debug">
            <div class="switcher-indicator">
              <div class="switcher-yes">YES</div>
              <div class="switcher-no">NO</div>
            </div>
          </label>
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-offset-2 col-md-10">
            @if(session('auth')->Witel_New)
                <button type="submit" class="btn"><i class="ion-soup-can"></i> Simpan</button>
            @else
                <span class="label label-danger">Witel Anda Terbaca Kosong, silahkan hubungi Admin.</span>
            @endif
        </div>
    </div>
</form>
</div>
</div>
@endsection

@section('js')
<script>
  $(function() {
    $('#form-upload').pxValidate();
});
</script>
@endsection
