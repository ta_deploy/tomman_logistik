@extends('layout')
@section('heading')
<h1>
  <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>RFC Bertandatangan / </span>upload
</h1>
@endsection
@section('title', 'Upload')
@section('content')
<div class="panel">
  <div class="panel-body">
<form class="form-horizontal" method="post" id="form-upload" enctype="multipart/form-data">
    <div class="form-group form-message-dark">
        <label for="RFC" class="col-md-2 control-label">RFC</label>
        <div class="col-md-10">
            <input type="file" id="rfc" class="form-control-file" name="rfc[]" required multiple>
            <small class="text-muted">RFC bertandatangan hasil scan. Penamaan File nya dengan format : NOMOR_RFC.pdf, contoh: <b>4902587382.pdf</b></small>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-offset-2 col-md-10">
            <button type="submit" class="btn"><i class="ion-soup-can"></i> Simpan</button>
        </div>
    </div>
</form>
</div>
</div>
@endsection

@section('js')
<script>
  $(function() {
    $('#form-upload').pxValidate();
});
</script>
@endsection
