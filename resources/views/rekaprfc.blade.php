@extends('layout')
@section('css')
<style type="text/css">
    .no-search .select2-search {
        display:none
    }
    body .modal-xl {
        width: 1250px;
    }
</style>
@endsection
@section('heading')

<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>RFC / </span>Matrik {{ date_format(date_create(Request::segment(2)), 'F Y') }}
</h1>
<!-- <a href="/downloads" class="pull-right"><span class="btn btn-info"><i class="ion ion-android-archive"></i> Download</span></a> -->
@endsection
@section('title', 'Matrik RFC')
@section('content')
<div class="panel">
  <div class="panel-body">
    <div class="table-responsive table-primary">
    <table class="table table-bordered">
        <thead>
            <tr>
                <th rowspan="2" class="valign-middle text-xs-center">TGL</th>
                <th colspan="3" class="valign-middle text-xs-center">RFC WH BJM</th>
                <th colspan="2" class="valign-middle text-xs-center">RFC SO BJM1</th>
                <th colspan="3" class="valign-middle text-xs-center">RFC SO BJM2</th>
                <th colspan="3" class="valign-middle text-xs-center">RFC SO BJB</th>
                <th colspan="3" class="valign-middle text-xs-center">RFC SO BLC</th>
                <th colspan="3" class="valign-middle text-xs-center">RFC SO TJL</th>
                <th colspan="3" class="valign-middle text-xs-center">RFC SO BRI</th>
                <th colspan="3" class="valign-middle text-xs-center">RFC SO KPL</th>
                <th colspan="3" class="valign-middle text-xs-center">RFC SO PLE</th>
                <th colspan="3" class="valign-middle text-xs-center">RFC SO STI</th>
                <th colspan="3" class="valign-middle text-xs-center">RFC SO AMT</th>
                <th colspan="3" class="valign-middle text-xs-center">RFC SO RTA</th>
                <th colspan="3" class="valign-middle text-xs-center">RFC SO KDG</th>
                <th colspan="3" class="valign-middle text-xs-center">TOTAL RFC</th>
            </tr>
            <tr>
                <th class="valign-middle text-xs-center">R</th>
                <th class="valign-middle text-xs-center">S</th>
                <th class="valign-middle text-xs-center">AL</th>
                <th class="valign-middle text-xs-center">R</th>
                {{-- <th class="valign-middle text-xs-center"</th> --}}
                <th class="valign-middle text-xs-center">AL</th>
                <th class="valign-middle text-xs-center">R</th>
                <th class="valign-middle text-xs-center">S</th>
                <th class="valign-middle text-xs-center">AL</th>
                <th class="valign-middle text-xs-center">R</th>
                <th class="valign-middle text-xs-center">S</th>
                <th class="valign-middle text-xs-center">AL</th>
                <th class="valign-middle text-xs-center">R</th>
                <th class="valign-middle text-xs-center">S</th>
                <th class="valign-middle text-xs-center">AL</th>
                <th class="valign-middle text-xs-center">R</th>
                <th class="valign-middle text-xs-center">S</th>
                <th class="valign-middle text-xs-center">AL</th>
                <th class="valign-middle text-xs-center">R</th>
                <th class="valign-middle text-xs-center">S</th>
                <th class="valign-middle text-xs-center">AL</th>
                <th class="valign-middle text-xs-center">R</th>
                <th class="valign-middle text-xs-center">S</th>
                <th class="valign-middle text-xs-center">AL</th>
                <th class="valign-middle text-xs-center">R</th>
                <th class="valign-middle text-xs-center">S</th>
                <th class="valign-middle text-xs-center">AL</th>
                <th class="valign-middle text-xs-center">R</th>
                <th class="valign-middle text-xs-center">S</th>
                <th class="valign-middle text-xs-center">AL</th>
                <th class="valign-middle text-xs-center">R</th>
                <th class="valign-middle text-xs-center">S</th>
                <th class="valign-middle text-xs-center">AL</th>
                <th class="valign-middle text-xs-center">R</th>
                <th class="valign-middle text-xs-center">S</th>
                <th class="valign-middle text-xs-center">AL</th>
                <th class="valign-middle text-xs-center">R</th>
                <th class="valign-middle text-xs-center">S</th>
                <th class="valign-middle text-xs-center">AL</th>
                <th class="valign-middle text-xs-center">R</th>
                <th class="valign-middle text-xs-center">S</th>
                <th class="valign-middle text-xs-center">AL</th>
            </tr>
        </thead>
        <tbody>
            <?php
                $totalall = $WH_BJM_RFC = $WHSO_BJM1_RFC = $WHSO_BJM2_RFC = $WHSO_BJB_RFC = $WHSO_BLC_RFC = $WHSO_TJL_RFC = $WHSO_BRI_RFC = $WHSO_KPL_RFC = $WHSO_PLE_RFC = $WHSO_STI_RFC = $WHSO_AMT_RFC = $WHSO_RTA_RFC = $WHSO_KDG_RFC = 0;
                $totalttdall = $WH_BJM_TTD = $WHSO_BJM1_TTD = $WHSO_BJM2_TTD = $WHSO_BJB_TTD = $WHSO_BLC_TTD = $WHSO_TJL_TTD = $WHSO_BRI_TTD = $WHSO_KPL_TTD = $WHSO_PLE_TTD = $WHSO_STI_TTD = $WHSO_AMT_TTD = $WHSO_RTA_TTD = $WHSO_KDG_TTD = 0;
            ?>
            @foreach($data as $no => $d)

            <?php
                $total        =  $d->WH_BJM_RFC + $d->WHSO_BJM1_RFC + $d->WHSO_BJM2_RFC + $d->WHSO_BJB_RFC + $d->WHSO_BLC_RFC + $d->WHSO_TJL_RFC + $d->WHSO_BRI_RFC + $d->WHSO_KPL_RFC + $d->WHSO_PLE_RFC + $d->WHSO_STI_RFC + $d->WHSO_AMT_RFC + $d->WHSO_RTA_RFC + $d->WHSO_KDG_RFC;
                $totalttd     =  $d->WH_BJM_TTD + $d->WHSO_BJM1_TTD + $d->WHSO_BJM2_TTD + $d->WHSO_BJB_TTD + $d->WHSO_BLC_TTD + $d->WHSO_TJL_TTD + $d->WHSO_BRI_TTD + $d->WHSO_KPL_TTD + $d->WHSO_PLE_TTD + $d->WHSO_STI_TTD + $d->WHSO_AMT_TTD + $d->WHSO_RTA_TTD + $d->WHSO_KDG_TTD;
                $totalall    += $total;
                $totalttdall += $totalttd;

                $WH_BJM_RFC    += $d->WH_BJM_RFC;
                $WHSO_BJM1_RFC += $d->WHSO_BJM1_RFC;
                $WHSO_BJM2_RFC += $d->WHSO_BJM2_RFC;
                $WHSO_BJB_RFC  += $d->WHSO_BJB_RFC;
                $WHSO_BLC_RFC  += $d->WHSO_BLC_RFC;
                $WHSO_TJL_RFC  += $d->WHSO_TJL_RFC;
                $WHSO_BRI_RFC  += $d->WHSO_BRI_RFC;
                $WHSO_KPL_RFC  += $d->WHSO_KPL_RFC;
                $WHSO_PLE_RFC  += $d->WHSO_PLE_RFC;
                $WHSO_STI_RFC  += $d->WHSO_STI_RFC;
                $WHSO_AMT_RFC  += $d->WHSO_AMT_RFC;
                $WHSO_RTA_RFC  += $d->WHSO_RTA_RFC;
                $WHSO_KDG_RFC  += $d->WHSO_KDG_RFC;

                $WH_BJM_TTD    += $d->WH_BJM_TTD;
                $WHSO_BJM1_TTD += $d->WHSO_BJM1_TTD;
                $WHSO_BJM2_TTD += $d->WHSO_BJM2_TTD;
                $WHSO_BJB_TTD  += $d->WHSO_BJB_TTD;
                $WHSO_BLC_TTD  += $d->WHSO_BLC_TTD;
                $WHSO_TJL_TTD  += $d->WHSO_TJL_TTD;
                $WHSO_KPL_TTD  += $d->WHSO_KPL_TTD;
                $WHSO_BRI_TTD  += $d->WHSO_BRI_TTD;
                $WHSO_PLE_TTD  += $d->WHSO_PLE_TTD;
                $WHSO_STI_TTD  += $d->WHSO_STI_TTD;
                $WHSO_AMT_TTD  += $d->WHSO_AMT_TTD;
                $WHSO_RTA_TTD  += $d->WHSO_RTA_TTD;
                $WHSO_KDG_TTD  += $d->WHSO_KDG_TTD;

                $invWH_BJM    = @$invarray[$d->TGL]->WH_BJM_RFC ?: 0;
                $invWHSO_BJM1 = @$invarray[$d->TGL]->WHSO_BJM1_RFC ?: 0;
                $invWHSO_BJM2 = @$invarray[$d->TGL]->WHSO_BJM2_RFC ?: 0;
                $invWHSO_BJB  = @$invarray[$d->TGL]->WHSO_BJB_RFC ?: 0;
                $invWHSO_BLC  = @$invarray[$d->TGL]->WHSO_BLC_RFC ?: 0;
                $invWHSO_TJL  = @$invarray[$d->TGL]->WHSO_TJL_RFC ?: 0;
                $invWHSO_BRI  = @$invarray[$d->TGL]->WHSO_BRI_RFC ?: 0;
                $invWHSO_KPL  = @$invarray[$d->TGL]->WHSO_KPL_RFC ?: 0;
                $invWHSO_PLE  = @$invarray[$d->TGL]->WHSO_PLE_RFC ?: 0;
                $invWHSO_STI  = @$invarray[$d->TGL]->WHSO_STI_RFC ?: 0;
                $invWHSO_AMT  = @$invarray[$d->TGL]->WHSO_AMT_RFC ?: 0;
                $invWHSO_RTA  = @$invarray[$d->TGL]->WHSO_RTA_RFC ?: 0;
                $invWHSO_KDG  = @$invarray[$d->TGL]->WHSO_KDG_RFC ?: 0;

            ?>
                <tr>
                    <td>{{ substr($d->TGL,8) }}</td>
                    <td>
                        <span style="cursor: pointer;" class="pull-right ajax" data-tgl="{{ $d->TGL }}" data-so="WH Banjarmasin" data-jenis="RFC">{{ number_format($d->WH_BJM_RFC) ?: '-' }}</span>
                    </td>
                    <td class="{{ ($d->WH_BJM_RFC == $d->WH_BJM_TTD)?'bg-success': 'bg-danger'}}">
                        <span style="cursor: pointer;" class="pull-right ajax" data-tgl="{{ $d->TGL }}" data-so="WH Banjarmasin" data-jenis="TTD">{{ number_format($d->WH_BJM_TTD) ?: '-' }}</span>
                    </td>
                    <td class="{{ ($invWH_BJM == $d->WH_BJM_TTD)?'bg-success': 'bg-danger'}}">
                        <span style="cursor: pointer;" class="pull-right ajax" data-tgl="{{ $d->TGL }}" data-so="Banjarmasin" data-jenis="ALISTA">{{ number_format($invWH_BJM) ?: '-' }}</span>
                    </td>

                    <td>
                        <span style="cursor: pointer;" class="pull-right ajax" data-tgl="{{ $d->TGL }}" data-so="WH SO Banjarmasin Centrum" data-jenis="RFC">{{ number_format($d->WHSO_BJM1_RFC) ?: '-' }}</span>
                    </td>
                    {{-- <td class="{{ ($d->WHSO_BJM1_RFC == $d->WHSO_BJM1_TTD)?'bg-success': 'bg-danger'}}">
                        <span style="cursor: pointer;" class="pull-right ajax" data-tgl="{{ $d->TGL }}" data-so="WH SO Banjarmasin Centrum" data-jenis="TTD">{{ number_format($d->WHSO_BJM1_TTD) ?: '-' }}</span></td
                        > --}}
                    <td class="{{ ($invWHSO_BJM1 == $d->WHSO_BJM1_RFC)?'bg-success': 'bg-danger'}}">
                        <span style="cursor: pointer;" class="pull-right ajax" data-tgl="{{ $d->TGL }}" data-so="SO Banjarmasin Centrum" data-jenis="ALISTA">{{ number_format($invWHSO_BJM1) ?: '-' }}</span>
                    </td>

                    <td>
                        <span style="cursor: pointer;" class="pull-right ajax" data-tgl="{{ $d->TGL }}" data-so="WH SO Banjarmasin A.Yani" data-jenis="RFC">{{ number_format($d->WHSO_BJM2_RFC) ?: '-' }}</span>
                    </td>
                    <td class="{{ ($d->WHSO_BJM2_RFC == $d->WHSO_BJM2_TTD)?'bg-success': 'bg-danger'}}">
                        <span style="cursor: pointer;" class="pull-right ajax" data-tgl="{{ $d->TGL }}" data-so="WH SO Banjarmasin A.Yani" data-jenis="TTD">{{ number_format($d->WHSO_BJM2_TTD) ?: '-' }}</span>
                    </td>
                    <td class="{{ ($invWHSO_BJM2 == $d->WHSO_BJM2_TTD)?'bg-success': 'bg-danger'}}">
                        <span style="cursor: pointer;" class="pull-right ajax" data-tgl="{{ $d->TGL }}" data-so="SO Banjarmasin A.Yani" data-jenis="ALISTA">{{ number_format($invWHSO_BJM2) ?: '-' }}</span>
                    </td>

                    <td>
                        <span style="cursor: pointer;" class="pull-right ajax" data-tgl="{{ $d->TGL }}" data-so="WH SO Banjarbaru" data-jenis="RFC">{{ number_format($d->WHSO_BJB_RFC) ?: '-' }}</span>
                    </td>
                    <td class="{{ ($d->WHSO_BJB_RFC == $d->WHSO_BJB_TTD)?'bg-success': 'bg-danger'}}">
                        <span style="cursor: pointer;" class="pull-right ajax" data-tgl="{{ $d->TGL }}" data-so="WH SO Banjarbaru" data-jenis="TTD">{{ number_format($d->WHSO_BJB_TTD) ?: '-' }}</span>
                    </td>
                    <td class="{{ ($invWHSO_BJB == $d->WHSO_BJB_TTD)?'bg-success': 'bg-danger'}}">
                        <span style="cursor: pointer;" class="pull-right ajax" data-tgl="{{ $d->TGL }}" data-so="SO Banjarbaru" data-jenis="ALISTA">{{ number_format($invWHSO_BJB) ?: '-' }}</span>
                    </td>

                    <td>
                        <span style="cursor: pointer;" class="pull-right ajax" data-tgl="{{ $d->TGL }}" data-so="WH SO Batulicin" data-jenis="RFC">{{ number_format($d->WHSO_BLC_RFC) ?: '-' }}</span>
                    </td>
                    <td class="{{ ($d->WHSO_BLC_RFC == $d->WHSO_BLC_TTD)?'bg-success': 'bg-danger'}}">
                        <span style="cursor: pointer;" class="pull-right ajax" data-tgl="{{ $d->TGL }}" data-so="WH SO Batulicin" data-jenis="TTD">{{ number_format($d->WHSO_BLC_TTD) ?: '-' }}</span>
                    </td>
                    <td class="{{ ($invWHSO_BLC == $d->WHSO_BLC_TTD)?'bg-success': 'bg-danger'}}">
                        <span style="cursor: pointer;" class="pull-right ajax" data-tgl="{{ $d->TGL }}" data-so="SO Batulicin" data-jenis="ALISTA">{{ number_format($invWHSO_BLC) ?: '-' }}</span>
                    </td>


                    <td>
                        <span style="cursor: pointer;" class="pull-right ajax" data-tgl="{{ $d->TGL }}" data-so="WH SO Tabalong" data-jenis="RFC">{{ number_format($d->WHSO_TJL_RFC) ?: '-' }}</span>
                    </td>
                    <td class="{{ ($d->WHSO_TJL_RFC == $d->WHSO_TJL_TTD)?'bg-success': 'bg-danger'}}">
                        <span style="cursor: pointer;" class="pull-right ajax" data-tgl="{{ $d->TGL }}" data-so="WH SO Tabalong" data-jenis="TTD">{{ number_format($d->WHSO_TJL_TTD) ?: '-' }}</span>
                    </td>
                    <td class="{{ ($invWHSO_TJL == $d->WHSO_TJL_TTD)?'bg-success': 'bg-danger'}}">
                        <span style="cursor: pointer;" class="pull-right ajax" data-tgl="{{ $d->TGL }}" data-so="SO Tabalong" data-jenis="ALISTA">{{ number_format($invWHSO_TJL) ?: '-' }}</span>
                    </td>


                    <td>
                        <span style="cursor: pointer;" class="pull-right ajax" data-tgl="{{ $d->TGL }}" data-so="WH SO Barabai" data-jenis="RFC">{{ number_format($d->WHSO_BRI_RFC) ?: '-' }}</span>
                    </td>
                    <td class="{{ ($d->WHSO_BRI_RFC == $d->WHSO_BRI_TTD)?'bg-success': 'bg-danger'}}">
                        <span style="cursor: pointer;" class="pull-right ajax" data-tgl="{{ $d->TGL }}" data-so="WH SO Barabai" data-jenis="TTD">{{ number_format($d->WHSO_BRI_TTD) ?: '-' }}</span>
                    </td>
                    <td class="{{ ($invWHSO_BRI == $d->WHSO_BRI_TTD)?'bg-success': 'bg-danger'}}">
                        <span style="cursor: pointer;" class="pull-right ajax" data-tgl="{{ $d->TGL }}" data-so="SO Barabai" data-jenis="ALISTA">{{ number_format($invWHSO_BRI) ?: '-' }}</span>
                    </td>

                    <td>
                        <span style="cursor: pointer;" class="pull-right ajax" data-tgl="{{ $d->TGL }}" data-so="WH SO Kotabaru2" data-jenis="RFC">{{ number_format($d->WHSO_KPL_RFC) ?: '-' }}</span>
                    </td>
                    <td class="{{ ($d->WHSO_KPL_RFC == $d->WHSO_KPL_TTD)?'bg-success': 'bg-danger'}}">
                        <span style="cursor: pointer;" class="pull-right ajax" data-tgl="{{ $d->TGL }}" data-so="WH SO Kotabaru2" data-jenis="TTD">{{ number_format($d->WHSO_KPL_TTD) ?: '-' }}</span>
                    </td>
                    <td class="{{ ($invWHSO_KPL == $d->WHSO_KPL_TTD)?'bg-success': 'bg-danger'}}">
                        <span style="cursor: pointer;" class="pull-right ajax" data-tgl="{{ $d->TGL }}" data-so="SO Kotabaru2" data-jenis="ALISTA">{{ number_format($invWHSO_KPL) ?: '-' }}</span>
                    </td>

                    <td>
                        <span style="cursor: pointer;" class="pull-right ajax" data-tgl="{{ $d->TGL }}" data-so="WH SO Pelaihari" data-jenis="RFC">{{ number_format($d->WHSO_PLE_RFC) ?: '-' }}</span>
                    </td>
                    <td class="{{ ($d->WHSO_PLE_RFC == $d->WHSO_PLE_TTD)?'bg-success': 'bg-danger'}}">
                        <span style="cursor: pointer;" class="pull-right ajax" data-tgl="{{ $d->TGL }}" data-so="WH SO Pelaihari" data-jenis="TTD">{{ number_format($d->WHSO_PLE_TTD) ?: '-' }}</span>
                    </td>
                    <td class="{{ ($invWHSO_PLE == $d->WHSO_PLE_TTD)?'bg-success': 'bg-danger'}}">
                        <span style="cursor: pointer;" class="pull-right ajax" data-tgl="{{ $d->TGL }}" data-so="SO Pelaihari" data-jenis="ALISTA">{{ number_format($invWHSO_PLE) ?: '-' }}</span>
                    </td>

                    <td>
                        <span style="cursor: pointer;" class="pull-right ajax" data-tgl="{{ $d->TGL }}" data-so="WH SO Satui" data-jenis="RFC">{{ number_format($d->WHSO_STI_RFC) ?: '-' }}</span>
                    </td>
                    <td class="{{ ($d->WHSO_STI_RFC == $d->WHSO_STI_TTD)?'bg-success': 'bg-danger'}}">
                        <span style="cursor: pointer;" class="pull-right ajax" data-tgl="{{ $d->TGL }}" data-so="WH SO Satui" data-jenis="TTD">{{ number_format($d->WHSO_STI_TTD) ?: '-' }}</span>
                    </td>
                    <td class="{{ ($invWHSO_STI == $d->WHSO_STI_TTD)?'bg-success': 'bg-danger'}}">
                        <span style="cursor: pointer;" class="pull-right ajax" data-tgl="{{ $d->TGL }}" data-so="SO Satui" data-jenis="ALISTA">{{ number_format($invWHSO_STI) ?: '-' }}</span>
                    </td>

                    <td>
                        <span style="cursor: pointer;" class="pull-right ajax" data-tgl="{{ $d->TGL }}" data-so="WH SO Amuntai" data-jenis="RFC">{{ number_format($d->WHSO_AMT_RFC) ?: '-' }}</span>
                    </td>
                    <td class="{{ ($d->WHSO_AMT_RFC == $d->WHSO_AMT_TTD)?'bg-success': 'bg-danger'}}">
                        <span style="cursor: pointer;" class="pull-right ajax" data-tgl="{{ $d->TGL }}" data-so="WH SO Amuntai" data-jenis="TTD">{{ number_format($d->WHSO_AMT_TTD) ?: '-' }}</span>
                    </td>
                    <td class="{{ ($invWHSO_AMT == $d->WHSO_AMT_TTD)?'bg-success': 'bg-danger'}}">
                        <span style="cursor: pointer;" class="pull-right ajax" data-tgl="{{ $d->TGL }}" data-so="SO Amuntai" data-jenis="ALISTA">{{ number_format($invWHSO_AMT) ?: '-' }}</span>
                    </td>

                    <td>
                        <span style="cursor: pointer;" class="pull-right ajax" data-tgl="{{ $d->TGL }}" data-so="WH SO Rantau" data-jenis="RFC">{{ number_format($d->WHSO_RTA_RFC) ?: '-' }}</span>
                    </td>
                    <td class="{{ ($d->WHSO_RTA_RFC == $d->WHSO_RTA_TTD)?'bg-success': 'bg-danger'}}">
                        <span style="cursor: pointer;" class="pull-right ajax" data-tgl="{{ $d->TGL }}" data-so="WH SO Rantau" data-jenis="TTD">{{ number_format($d->WHSO_RTA_TTD) ?: '-' }}</span>
                    </td>
                    <td class="{{ ($invWHSO_RTA == $d->WHSO_RTA_TTD)?'bg-success': 'bg-danger'}}">
                        <span style="cursor: pointer;" class="pull-right ajax" data-tgl="{{ $d->TGL }}" data-so="SO Rantau" data-jenis="ALISTA">{{ number_format($invWHSO_RTA) ?: '-' }}</span>
                    </td>

                    <td>
                        <span style="cursor: pointer;" class="pull-right ajax" data-tgl="{{ $d->TGL }}" data-so="WH SO Kandangan" data-jenis="RFC">{{ number_format($d->WHSO_KDG_RFC) ?: '-' }}</span>
                    </td>
                    <td class="{{ ($d->WHSO_KDG_RFC == $d->WHSO_KDG_TTD)?'bg-success': 'bg-danger'}}">
                        <span style="cursor: pointer;" class="pull-right ajax" data-tgl="{{ $d->TGL }}" data-so="WH SO Kandangan" data-jenis="TTD">{{ number_format($d->WHSO_KDG_TTD) ?: '-' }}</span>
                    </td>
                    <td class="{{ ($invWHSO_KDG == $d->WHSO_KDG_TTD)?'bg-success': 'bg-danger'}}">
                        <span style="cursor: pointer;" class="pull-right ajax" data-tgl="{{ $d->TGL }}" data-so="SO Kandangan" data-jenis="ALISTA">{{ number_format($invWHSO_KDG) ?: '-' }}</span>
                    </td>

                    <td><b style="cursor: pointer;" class="pull-right ajax" data-tgl="{{ $d->TGL }}" data-so="0" data-jenis="RFC">{{ number_format($total) ?: '-' }}</b></td>
                    <td><b style="cursor: pointer;" class="pull-right ajax" data-tgl="{{ $d->TGL }}" data-so="0" data-jenis="TTD">{{ number_format($totalttd) ?: '-' }}</b></td>
                    <td><b style="cursor: pointer;" class="pull-right">{{ number_format(@$invarray[$d->TGL]->TOTAL_RFC) ?: '-' }}</b></td>
                </tr>
            @endforeach
            <tr>
                <td colspan="1">TOTAL</td>
                <td><span style="cursor: pointer;" class="pull-right">{{ number_format($WH_BJM_RFC) }}</span></td>
                <td><span style="cursor: pointer;" class="pull-right">{{ number_format($WH_BJM_TTD) }}</span></td>
                <td><span class="pull-right">-</span></td>
                <td><span style="cursor: pointer;" class="pull-right">{{ number_format($WHSO_BJM1_RFC) }}</span></td>
                <td><span style="cursor: pointer;" class="pull-right">{{ number_format($WHSO_BJM1_TTD) }}</span></td>
                <td><span class="pull-right">-</span></td>
                <td><span style="cursor: pointer;" class="pull-right">{{ number_format($WHSO_BJM2_RFC) }}</span></td>
                <td><span style="cursor: pointer;" class="pull-right">{{ number_format($WHSO_BJM2_TTD) }}</span></td>
                <td><span style="cursor: pointer;" class="pull-right">{{ number_format($WHSO_BJB_RFC) }}</span></td>
                <td><span style="cursor: pointer;" class="pull-right">{{ number_format($WHSO_BJB_TTD) }}</span></td>
                <td><span class="pull-right">-</span></td>
                <td><span style="cursor: pointer;" class="pull-right">{{ number_format($WHSO_BLC_RFC) }}</span></td>
                <td><span style="cursor: pointer;" class="pull-right">{{ number_format($WHSO_BLC_TTD) }}</span></td>
                <td><span class="pull-right">-</span></td>
                <td><span style="cursor: pointer;" class="pull-right">{{ number_format($WHSO_TJL_RFC) }}</span></td>
                <td><span style="cursor: pointer;" class="pull-right">{{ number_format($WHSO_TJL_TTD) }}</span></td>
                <td><span class="pull-right">-</span></td>
                <td><span style="cursor: pointer;" class="pull-right">{{ number_format($WHSO_BRI_RFC) }}</span></td>
                <td><span style="cursor: pointer;" class="pull-right">{{ number_format($WHSO_BRI_TTD) }}</span></td>
                <td><span class="pull-right">-</span></td>
                <td><span style="cursor: pointer;" class="pull-right">{{ number_format($WHSO_KPL_RFC) }}</span></td>
                <td><span style="cursor: pointer;" class="pull-right">{{ number_format($WHSO_KPL_TTD) }}</span></td>
                <td><span class="pull-right">-</span></td>
                <td><span style="cursor: pointer;" class="pull-right">{{ number_format($WHSO_PLE_RFC) }}</span></td>
                <td><span style="cursor: pointer;" class="pull-right">{{ number_format($WHSO_PLE_TTD) }}</span></td>
                <td><span class="pull-right">-</span></td>
                <td><span style="cursor: pointer;" class="pull-right">{{ number_format($WHSO_STI_RFC) }}</span></td>
                <td><span style="cursor: pointer;" class="pull-right">{{ number_format($WHSO_STI_TTD) }}</span></td>
                <td><span class="pull-right">-</span></td>
                <td><span style="cursor: pointer;" class="pull-right">{{ number_format($WHSO_AMT_RFC) }}</span></td>
                <td><span style="cursor: pointer;" class="pull-right">{{ number_format($WHSO_AMT_TTD) }}</span></td>
                <td><span class="pull-right">-</span></td>
                <td><span style="cursor: pointer;" class="pull-right">{{ number_format($WHSO_RTA_RFC) }}</span></td>
                <td><span style="cursor: pointer;" class="pull-right">{{ number_format($WHSO_RTA_TTD) }}</span></td>
                <td><span class="pull-right">-</span></td>
                <td><span style="cursor: pointer;" class="pull-right">{{ number_format($WHSO_KDG_RFC) }}</span></td>
                <td><span style="cursor: pointer;" class="pull-right">{{ number_format($WHSO_KDG_TTD) }}</span></td>
                <td><span class="pull-right">-</span></td>
                <td><b class="pull-right">{{ number_format($totalall) }}</b></td>
                <td><b class="pull-right">{{ number_format($totalttdall) }}</b></td>
                <td><span class="pull-right">-</span></td>
            </tr>
        </tbody>
    </table>
</div>
</div>
</div>
<div class="modal" id="modal-info">
  <div class="modal-dialog modal-xl" >
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h4 class="modal-title tittle">List RFC</h4>
      </div>
      <div class="modal-body" id="detilContent">
        <p>One fine body…</p>
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
<script type="text/javascript">
    $(function(){
        $(".ajax").click(function(){
            var so = this.dataset.so;
            var tgl = this.dataset.tgl;
            var jenis = this.dataset.jenis;
            $.get('/ajaxrekaprfc/'+tgl+'/'+so+'/'+jenis, function(r){
                $('#detilContent').html(r);
                $("#modal-info").modal('show');
            });
        });
    });
</script>
@endsection
