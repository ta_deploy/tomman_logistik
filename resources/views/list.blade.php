@extends('layout')
@section('css')
<style type="text/css">
    .no-search .select2-search {
        display:none
    }
    body .modal-xl {
        width: 1250px;
    }
</style>
@endsection
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>Material / </span>List
</h1>

<button id="daterange-4" class="btn dropdown-toggle"></button>
<a href="/rfcttd" class="pull-right"><span class="btn btn-info"><i class="ion ion-plus"></i> Upload RFC Scan TTD</span></a>
<a href="/uploads" class="pull-right"><span class="btn btn-success m-r-1"><i class="ion ion-plus"></i> Upload RFC Asli SAP</span></a>
@endsection
@section('title', 'List Material')
@section('content')

@if (@explode(':', Request::segment(2))[0])
    <input type="hidden" name="start" id="start" value="{{ @explode(':', Request::segment(2))[0] }}">
    <input type="hidden" name="end" id="end" value="{{ @explode(':', Request::segment(2))[1] }}">
@endif

<div class="panel">
  <div class="panel-body">
    <div class="table-responsive table-primary">
        <table class="table">
            <thead>
                <tr>
                    <th>INFO</th>
                    <th>PENGESAHAN</th>
                    <th>ID BARANG</th>
                    <th>MINTA</th>
                    <th>BERI</th>
                </tr>
            </thead>
            <tbody>
                    <?php
                    $no=0;
                    ?>
                @foreach($dataarray as $key => $d)

                    <?php
                        $span = count($d->list);
                        $path = '/storage/rfc_ttd/'.$d->RFC.'.pdf';
                        $pathori = '/storage/rfc/'.$d->FILE_RFC;
                        $path2 = '/storage2/rfc_ttd/'.$d->RFC.'.pdf';
                        $pathori2 = '/storage2/rfc/'.$d->FILE_RFC;
                    ?>
                    <tr>
                        <td rowspan="{{ $span }}">
                            <span class="label label-info">{{ ++$no }} . </span>
                            <span class="label label-primary">RFC : {{ $d->RFC }}</span><br>
                            <span class="label label-primary">PID : {{ $d->PID }}</span><br>
                            <span class="label label-primary">{{ $d->NAMA_PROJECT }}</span><br>
                            <span class="label label-primary">GUDANG : {{ $d->NAMA_GUDANG }}</span><br>
                            <span class="label label-primary">{{ $d->MITRA }}</span><br>
                            <span class="label label-primary">LOKASI : {{ $d->LOKASI }}</span>
                        </td>
                        <td rowspan="{{ $span }}">
                            @if (file_exists(public_path().$pathori))
                                <a href="{{ $pathori }}" target="_blank"><span class="btn btn-xs btn-success"><i class="ion ion-ios-cloud-download"></i> Download RFC ASLI</span></a>
                            @else
                                <a href="{{ $pathori2 }}" target="_blank"><span class="btn btn-xs btn-success"><i class="ion ion-ios-cloud-download"></i> Download RFC ASLI</span></a>
                            @endif
                            @if (file_exists(public_path().$path))
                                <a href="{{ $path }}" target="_blank"><span class="btn btn-xs btn-success"><i class="ion ion-ios-cloud-download"></i> Download RFC TTD</span></a><br>
                            @elseif (file_exists(public_path().$path2))
                                <a href="{{ $path2 }}" target="_blank"><span class="btn btn-xs btn-success"><i class="ion ion-ios-cloud-download"></i> Download RFC TTD</span></a><br>
                            @else
                                <span class="label label-danger">RFC Bertandatangan BLM ADA</span><br>
                            @endif
                            <span class="label label-primary">TGL : {{ $d->TGL }}</span><br>
                            <span class="label label-primary">MENYERAHKAN : {{ $d->MENYERAHKAN }}</span><br>
                            <span class="label label-primary">MENERIMA : {{ $d->MENERIMA }}</span><br>
                            <span class="label label-primary">MENGETAHUI : {{ $d->MENGETAHUI }}</span>

                        </td>
                        @foreach($d->list as $nos => $l)
                            @if($nos)
                            <tr>
                            @endif
                                <td class="valign-middle">
                                    <span class="label label-success">{{ $l['ID_BARANG'] }}</span> <span class="label label-default">{{ $l['SATUAN'] }}</span><br>
                                    <span class="label label-primary">{{ $l['NAMA_BARANG'] }}</span>
                                </td>
                                <td class="valign-middle"><span class="pull-right">{{ number_format($l['MINTA']) }}</span></td>
                                <td class="valign-middle"><span class="pull-right">{{ number_format($l['BERI']) }}</span></td>
                            </tr>
                        @endforeach

                @endforeach
            </tbody>
        </table>
    </div>
  </div>
</div>

@endsection

@section('js')
<script type="text/javascript">
    $(function() {
        var start = moment($('#start').val());
        var end = moment($('#end').val());

        function cb(start, end) {
          $('#daterange-4').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }

        $('#daterange-4').daterangepicker({
          startDate: start,
          endDate: end,
          ranges: {
           'Hari Ini': [moment(), moment()],
           'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           '7 Hari Terakhir': [moment().subtract(6, 'days'), moment()],
           '30 Hari Terakhir': [moment().subtract(29, 'days'), moment()],
           'Bulan Ini': [moment().startOf('month'), moment().endOf('month')],
           'Bulan Sebelumnya': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          }
        }, cb);

        cb(start, end);
        $('#daterange-4').on('apply.daterangepicker', function(ev, picker) {
            var s = picker.startDate.format('YYYY-MM-DD');
            var e = picker.endDate.format('YYYY-MM-DD');
            window.location.href = document.location.origin+"/rfc/"+s+":"+e;
        });
    });
</script>
@endsection
