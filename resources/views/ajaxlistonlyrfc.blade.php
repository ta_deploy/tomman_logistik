<div class="table-responsive table-primary">
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>#</th>
                <th>RFC</th>
                <th>STATUS</th>
            </tr>
        </thead>
        <tbody>
            @foreach($data as $key => $d)
                <tr>
                    <td>
                        {{ ++$key }}
                    </td>
                    <td>{{ $d->no_rfc }}</td>
                    <td>{{ $d->existed?'SUDAH UPLOAD':'BLM UPLOAD' }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>