<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', function () {
    return view('login');
});
Route::post('/login', 'UserController@login');

Route::group(['middleware' => 'logistik'], function () {
    Route::get('/', 'RFCController@home');
    Route::get('/search', 'RFCController@listrfc');
    //rfc
    Route::get('/rfc/{tgl}', 'RFCController@listrfc');
    Route::get('/uploads', 'RFCController@uploadForm');
    Route::post('/uploads', 'RFCController@uploadSave');
    Route::get('/rfcttd', 'RFCController@uploadFormTTD');
    Route::post('/rfcttd', 'RFCController@uploadSaveTTD');

    //rfr
    Route::get('/rfr/{tgl}', 'RFRController@listrfr');
    Route::get('/uploadrfr', 'RFRController@uploadForm');
    Route::post('/uploadrfr', 'RFRController@uploadSave');


    Route::get('/rekap_pengeluaran/{tgl}/{mitra}/{pid}', 'RekapController@rekap_pengeluaran');
    Route::get('/rekaprfc/{bln}', 'RekapController@rekap_pengeluaran_by_rfc');
    Route::get('/ajaxrekaprfc/{tgl}/{so}/{jenis}', 'RekapController@ajaxrekaprfc');
    Route::get('/dashboardrfc/{tgl}', 'DashboardRFCController@dashboardrfc');
    Route::get('/downloads/{tgl}/{mitra}/{pid}', 'RekapController@download');
    Route::get('/downloadspdfrar/{tgl}/{mitra}/{pid}', 'RekapController@downloadpdfrar');

    Route::get('/theme', 'UserController@themes');
    Route::post('/theme', 'UserController@themeSave');
    Route::get('/logout', 'UserController@logout');
});